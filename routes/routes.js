var express = require("express");
var app = express();
var fs = require('fs');
var router = express.Router();
var pg = require('pg');
var executePSqlQuery = require('../lib/psql');
var paymentFunctions = require('../lib/payments');
var Promise = require("bluebird");
var sendpic = require('../picfunctions/sendpic');
var auth = require('../auth/authentication.js').auth;
var auth_crypto = require('../auth/auth_helper');
var nodemailer = require('nodemailer');
var queries = require('../queries/queries.js').queries;
var mod = require('../themod/index');
var moment = require('moment');
var sec = "#3c8F5";
var sec1 = "5u110^";
var crypto = require('crypto');
var path = require('path');
var multer  = require('multer');
var http = require('http');
var requestlib = require('request');
var querystring = require('querystring');
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, '/Users/udaykrishna/code/yo/newwb/uploads')
    },
    filename: function (req, file, cb) {
        cb(null, Math.floor((Math.random() * 1000000000) + 1) + Date.now() + path.extname(file.originalname))
    }
});

var redis = require('promise-redis')();
var client = "";
if(process.env.ENV === "prod"){
    var client = redis.createClient();
} else {
    var client = redis.createClient(6379, "35.154.139.170");
}

var upload = multer({ storage: storage });

router.get('/', function(req, res) {
    res.render('home', {
        foo : "ih",
        bar : "bar"
    });
});

router.get('/test', function (req, res) {
    var x = {
        name: "Uday",
        occupation: "something"
    };
    res.json(x);
});

router.get('/contact', function(req, res, next){
    res.render('contact');
});

router.get('/consult', function(req, res, next){
    console.log("wtf");
    res.render('consultform', {
        foo : JSON.stringify(res.locals.user)
    });
});

router.get('/postpayment', function(req, res, next){
    //redis.
    //get case id and add it

// write data to request body
    // TODO : get specialization and then add it
    console.log("here");
    //console.log(req);
    console.log(req.query);
    if(!req.query.payment_request_id || !req.query.payment_id){
        //TODO : throw error
    }
    var payment_request_id = req.query.payment_request_id;
    var payment_id = req.query.payment_id;
    executePSqlQuery.pSqlPromise(queries.payment_status(payment_request_id, payment_id)).then(function (result) {
        if(result.length > 0){
            var status = result[0].status;
            if(status === "Credit"){
                var case_id = result[0].case_id;
                res.redirect('/cases?case='+case_id);
            } else {
                res.redirect('/payment-failure');
            }
        } else {
            var headers = {
                'X-Api-Key': '06902bfb1b24f467f47c8778635ac73d',
                'X-Auth-Token': 'cb196c9311a28e955fab2f4dc0dfcf52'
            }
            requestlib.get(
                'https://www.instamojo.com/api/1.1/payment-requests/'+payment_request_id+'/'+payment_id+'/',
                {
                    form: payload,  headers: headers
                }, function(error, response, body){
                    if(error){
                        res.send("wrong page");
                    }

                    body = JSON.parse(body);

                    console.log(body);

                    if(!body.success){
                        res.send("wrong page");
                    }

                    var payment_request = body.payment_request;

                    paymentFunctions.update_payments_promise(payment_request.id, payment_request.payment.payment_id, payment_request.payment.fees, payment_request.payment.amount, payment_request.payment.status, payment_request.payment.currency, payment_request.payment.created_at).then(function(update_payments_result){
                        if(update_payments_result.status === "Credit"){
                            res.redirect('/cases?case='+update_payments_result.case_id);
                        } else {
                            res.redirect('/payment-failure');
                        }
                    }).catch(function(update_payments_error){
                        res.send("wrong page");
                    });
            });
        }
    }).catch(function(post_payment_error){
        res.send('Payment failed');
    });
});

router.post('/consult1', upload.single('avatar'), function(req, res, next){
    console.log(req.body);
    console.log(req.file);
    console.log(res.locals.user);
    if (parseInt(req.body.illness) >= 0 && parseInt(req.body.illness) < illness[req.body.specialist].length) {
      req.body.illness = illness[req.body.specialist][parseInt(req.body.illness)];
    } else {
      req.body.illness = req.body.other;
    }
    console.log(req.body.illness);
    if(!req.body.illness){
      res.render('consultform');
    }
    //req.file.filename = req.file != undefined ? req.file.filename : '';
    if(req.file === undefined){
        req.file = {
            filename : ''
        }
    }
    auth.add_symptoms_for_consultation(req, res).then(function (result) {
        console.log(result);
        console.log("chutiya gaye hai bc1");
        console.log(result[0].id);

        executePSqlQuery.pSqlPromise(queries.get_case_patient_details(result[0].id)).then(function (data) {
            console.log(data);
            if(data.length > 0){
                var hostname = "http://35.154.139.170:50101/";
                var options = {
                    method: 'POST',
                    url: 'https://www.instamojo.com/api/1.1/payment-requests/',
                    headers:
                        {
                            'x-auth-token': 'cb196c9311a28e955fab2f4dc0dfcf52',
                            'x-api-key': '06902bfb1b24f467f47c8778635ac73d',
                            'content-type': 'application/x-www-form-urlencoded'
                        },
                    form:
                        {
                            allow_repeated_payments: 'True',
                            amount: '10',
                            buyer_name: data[0].name.trim(),
                            purpose: result[0].id,
                            redirect_url: hostname+'postpayment',
                            phone: data[0].username.trim(),
                            send_email: 'False',
                            webhook: hostname+'instamojo_webhook',
                            send_sms: 'False',
                            email: data[0].email.trim()
                        }
                    };

                requestlib(options, function (error, response, body) {
                    if (error) {
                        res.redirect('/payment-failure');
                        return;
                    }
                    console.log(body);
                    body = JSON.parse(body);
                    if(!body || body.success === false){
                        res.redirect('/payment-failure');
                    };

                    executePSqlQuery.pSqlPromise(queries.create_new_payment(body.payment_request.id, body.payment_request.buyer_name, body.payment_request.email, body.payment_request.phone, body.payment_request.longurl, body.payment_request.shorturl, body.payment_request.redirect_url, body.payment_request.webhook, body.payment_request.created_at, body.payment_request.amount, result[0].id)).then(function (data) {
                        res.redirect(body.payment_request.longurl);
                    }).catch(function(insert_payment_error){
                        res.send('Payment system failed');
                        //TODO ; notify admin
                    });
                });

            } else {
                res.redirect('/payment-failure');
            }
        }).catch(function(get_user_error){
            res.redirect('/payment-failure');
        });
    }).catch(function (error) {
        res.redirect('/payment-failure');
    });
});

router.post('/consult', upload.single('avatar'), function(req, res, next){
    console.log(req.body);
    console.log(req.file);
    console.log(res.locals.user);
    if (parseInt(req.body.illness) >= 0 && parseInt(req.body.illness) < illness[req.body.specialist].length) {
      req.body.illness = illness[req.body.specialist][parseInt(req.body.illness)];
    } else {
      req.body.illness = req.body.other;
    }
    console.log(req.body.illness);
    if(!req.body.illness){
      res.render('consultform');
    }
    //req.file.filename = req.file != undefined ? req.file.filename : '';
    if(req.file === undefined){
        req.file = {
            filename : ''
        }
    }
    auth.add_symptoms_for_consultation(req, res).then(function (result) {
        console.log(result);
        console.log("chutiya gaye hai bc");
        console.log(result[0].id);
        res.redirect('/cases?case='+result[0].id);
    }).catch(function (error) {
        res.redirect('/payment-failure');
    });
});

router.get('/payment-failure', function(req, res){
    res.render('payment-failure');
});

router.get('/signup', function(req, res) {
    console.log("hello form the other side");
    res.render('signup', {
        foo : "foo"
    });
    console.log("hello form the other side1");
});

router.post('/signup', function(req, res){
    auth.validate_and_sign_up_user(req.body.name, req.body.username, req.body.email, req.body.password, req.body.cpassword).then(function(result){
        if(result.cookie){
            res.cookie('mycookie', result.cookie, { maxAge: 9000000, httpOnly: false});
            res.redirect('consult');
        }else {
            console.log(result);
            res.render('signup', {
                foo : result
            });
        }
    }).catch(function(result){
        console.log(result);
        res.redirect('500');
    });
});

router.get('/signin', function(req, res){
    res.render('signin', {
        foo : "foo"
    });
});

router.get('/logout', function(req, res){
    res.clearCookie('mycookie');
    res.redirect('/');
});

router.post('/signin', function(req, res){
    auth.sign_user_in(req.body.username, req.body.password).then(function(result){
        if(result.cookie){
            res.cookie('mycookie', result.cookie, { maxAge: 9000000, httpOnly: false});
            res.redirect('consult');
        }else {
            res.render('signin', {
                foo : result
            });
        }
    }).catch(function(result){
        res.redirect('500');
    });
});

router.get('/keys', (req, res, next) => {
    executePSqlQuery.pSqlPromise(queries.get_keys_for_user(req.cookies.mycookie)).then(function(results){
        console.log(results);
        res.render('keys', {
            foo: results[0].aeskey,
            bar: results[0].token
        });
    }).catch(function (error) {
        res.redirect('500');
    });
});

router.get('/pic', function(req, res, next){
    console.log(mod.encrypt(JSON.stringify({uday:'uday'}), '68fcb168f28149588ed19012c0d3cf10bdbc8618'));
    var img = fs.readFileSync(path.join(__dirname, '../public/f60.png'));
    console.log(path.join(__dirname, '../public/f60.png'));
    res.writeHead(200, {'Content-Type': 'image/png' });
    res.end(img, 'binary');
    var d = req.query.d;
    var t = req.query.t;
    var query = queries.find_user_from_token(t);
    executePSqlQuery.pSqlPromise(query).then(function(results){
        if(results.length <= 0){
            console.log("no such user");
        }
        try{
            var decrypted = mod.decrypt(d, results[0].aeskey);
            var djson = JSON.parse(decrypted);
            console.log(djson);
            var enterQ = queries.insert_into_table_pic(decrypted, results[0].user_id);
            executePSqlQuery.pSqlPromise(enterQ);
        }catch (e){
            console.log("cant parse");
        }
    }).catch(function(error){
        console.log(error);
    });
});

router.get('/db', function (req, res) {
    console.log("SELECT * FROM test_table");
    var rows = executePSqlQuery.pSqlPromise('SELECT * FROM test_table').then(function(result){
        res.send({results : result});
    }).catch(function(err){
        console.log(err);
        res.send(err);
    });
});

router.get('/redirect', function(req, res, next){
    var r = req.query.r || '/';
    res.redirect(r);
    var d = req.query.d;
    var t = req.query.t;
    var query = queries.find_user_from_token(t);
    executePSqlQuery.pSqlPromise(query).then(function(results){
        if(results.length <= 0){
            console.log("no such user");
        }
        try{
            var decrypted = auth_crypto.decryptAES(d,results[0].aeskey);
            var djson = JSON.parse(decrypted);
            var enterQ = queries.insert_into_table_pic(decrypted, results[0].user_id);
            executePSqlQuery.pSqlPromise(enterQ);
        }catch (e){
            console.log("cant parse");
        }
    }).catch(function(error){
        console.log(error);
    });
});

router.get('/500', (req, res, next) => {
    res.render('500', {
        foo : JSON.stringify(req.user)
    });
});

router.get('/picstats', (req, res, next) => {
    var query = queries.get_pic_stats_of_a_user(req.cookies.mycookie);
    executePSqlQuery.pSqlPromise(query).then(function(results){
        res.render('stats', {
            foo: JSON.stringify(results),
            bar: "hi"
        });
    }).catch(function(error){
        res.redirect('500');
    });
});

router.get('/refstats', (req, res, next) => {
    var query = queries.get_ref_stats_of_a_user(req.cookies.mycookie);
    executePSqlQuery.pSqlPromise(query).then(function(results){
        res.render('stats', {
            foo: JSON.stringify(results),
            bar: "hi"
        });
    }).catch(function(error){
        res.redirect('500');
    });
});

router.get('/campaigns', function (req, res, next) {
    var query = queries.get_campaigns(res.locals.user.user_id);
    executePSqlQuery.pSqlPromise(query).then(function(results){
        console.log(res.locals.user.token);
        for(var i=0; i<results.length ; i++){
            results[i].token = res.locals.user.token;
        }
        res.render('campaigns', {
            foo: results
        });
    }).catch(function (err) {
        res.render('campaigns');
    });
});

router.post('/new_campaign', function (req, res, next) {
    var secret = crypto.randomBytes(3).toString('hex');
    var hash = {
        [secret] : res.locals.user.user_id
    };
    var keys = queries.get_keys_for_user(req.cookies.mycookie);
    executePSqlQuery.pSqlPromise(keys).then(function(results){
        var key = mod.encrypt(JSON.stringify(hash), results[0].aeskey);
        var date = new Date();
        var timestamp = moment().valueOf(date);
        var query = queries.create_new_campaign(res.locals.user.user_id, req.body.name, req.body.notes, key, secret, timestamp);
        executePSqlQuery.pSqlPromise(query).then(function(results){
            res.redirect('campaigns');
        }).catch(function (err) {
            res.redirect('campaigns');
        });
    }).catch(function (err) {
        res.redirect('campaigns');
    });
});

router.get('/g', function (req, res, next) {
    res.render('useless');
});

router.get('/cases1', function(req, res, next){
    var case_id = req.query.case;
    console.log(req.query);
    executePSqlQuery.pSqlPromise(queries.get_case_page_case_details(case_id)).then((result) => {
      if (result.length <= 0) {
        res.redirect('home');
        res.end();
        } else{
            console.log(result);
            executePSqlQuery.pSqlPromise(queries.add_to_cases(case_id)).then(function (data) {
                var options = {
                    method: 'POST',
                    url: 'http://localhost:50102/new_consult_arrived',
                    form:
                        {
                            caseid : case_id
                        }
                    };
                    requestlib(options, function(error, response, body){
                        if(error){
                            console.log(error);
                        }
                        res.render('casepage',{
                          name : result[0].name.trim(),
                          phonenumber : result[0].phonenumber.trim(),
                          illness : "Fever",
                          description : result[0].description.trim()
                        });
                    });
            }).catch(function (data) {
                console.log(data);
            });
        }
    }).catch((error) => {
      res.redirect('500');
    });
});

router.get('/cases', function(req, res, next){
    var case_id = req.query.case;
    console.log(req.query);
    var trigger_new_case = function(case_id){
        executePSqlQuery.pSqlPromise(queries.add_to_cases(case_id)).then(function (data) {
            var options = {
                method: 'POST',
                url: 'http://localhost:50102/new_consult_arrived',
                form:
                    {
                        caseid : case_id
                    }
                };
                requestlib(options, function(error, response, body){
                    if(error){
                        console.log(error);
                    }
                });
        }).catch(function (data) {
            console.log(data);
        });
    };
    executePSqlQuery.pSqlPromise(queries.get_case_page_case_details(case_id)).then((result) => {
      if (result.length <= 0) {
        res.redirect('home');
        res.end();
        } else{
            console.log(result);
            trigger_new_case(case_id);
            res.render('casepage',{
              name : result[0].name.trim(),
              phonenumber : result[0].phonenumber.trim(),
              illness : "Fever",
              description : result[0].description.trim()
            });
        }
    }).catch((error) => {
      res.redirect('500');
    });
});

router.post('/instamojo_webhook', function(req, res, next){
    console.log(req.body);
    //TODO : setup mac auth over here
    paymentFunctions.update_payments_promise(req.body.payment_request_id, req.body.payment_id, req.body.fees, req.body.amount, req.body.status, req.body.shorturl, req.body.currency, moment().toJSON());
    console.log(req.body);
    res.send("OK");;
});

router.get('/get_illnesses', function(req, res){
  res.send(findIllnesses(req.query.specialist));
});

var illness = {
  GP : ["Fever", "Cough", "Cold", "Weight Management"],
  GY : ["General", "Pregnency Issues", "PCOD", "Weight Management"],
  DL : ["Skin and Hair problems"],
  CL : ["Skin and Hair problems"],
  SX : [],
  PS : ["Stress", "Depression", "Psychological Counselling", "Career Counselling", "Children behavior Counselling", "Parent Child Counselling", "Family Conflict Counselling", "Relationship Counselling", "Rehabitilation Counselling", "Performance Counselling", "Anxiety Counselling", "Suicide Counselling", "Health Counselling", "Social  Counselling", "Fear Counselling", "Personal Counselling", "Pregnency Counselling", "Emotional Counselling", "Adolescent Counselling", "Pre marriage Counselling", "Post marriage Counselling"],
  VE : ["Dogs", "Cats"],
  PC : ["Nose Surgery", "Lip Surgery", "Mole Removal", "Ear Surgery", "Chin Augmentaion", "Arm Lift", "Laser Resurfacing"],
  CI : ["Pediatrics", "Child Care"],
  NU : ["Health and Fitness", "Weight Management", "Skin Rejuvenation", "Hair Growth"]
};

var physicians = {
  GP : "General Physician",
  GY : "Gynecologist",
  DL : "Dermatologist",
  CL : "Cosmotologist",
  SX : "Sexologist",
  PS : "Psychologist",
  VE : "Veterinarian",
  PC : "Plastic/Cosmetic Surgeon",
  CI : "Pediatrician",
  NU : "Nutritionist"
};

var findIllnesses = function(specialist){
  var selectList = '<select class="form-control illness-select" name="illness" onchange="illnessSelected(this);">';
  illness[specialist].forEach(function(elem, index){
    selectList = selectList + '<option value="'+index+'">'+elem+'</option>';
  });
  selectList = selectList + '<option value="-1">Other</option></select>';
  return selectList;
}

module.exports.router = router;
