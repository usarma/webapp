var cryptoAES = require('crypto'),
    algorithm = 'aes-256-ctr';

var decrypt = function (text, password) {
    var decipher = cryptoAES.createDecipher(algorithm,password);
    var dec = decipher.update(text,'hex','utf8');
    dec += decipher.final('utf8');
    return dec;
};
    
var encrypt = function (text, password) {
    var cipher = cryptoAES.createCipher(algorithm,password);
    var crypted = cipher.update(text,'utf8','hex');
    crypted += cipher.final('hex');
    return crypted;
};

module.exports.decrypt = decrypt;
module.exports.encrypt = encrypt;