var executePSqlQuery = require('../lib/psql');
var authHelper = require('../auth/auth_helper.js');
var crypto = require('crypto');
var xssFilters = require('xss-filters');
var moment = require('moment');

var queries = {
    find_user_from_token : function(tok){
        var psql_query = "SELECT user_id, aeskey, username FROM users WHERE token='"+tok+"'";
        return psql_query;
    },

    insert_into_table_pic : function(string, user_id){
        var string = xssFilters.inHTMLData(string);
        var psql_query = "INSERT INTO pic (string, user_id, timestamp) values ('"+string+"', '"+user_id+"', extract(epoch from now()))";
        return psql_query;
    },

    insert_into_table_ref : function(string, user_id){
        var string = xssFilters.inHTMLData(string);
        var psql_query = "INSERT INTO ref (string, user_id, timestamp) values ('"+string+"', '"+user_id+"', extract(epoch from now()))";
        return psql_query;
    },

    record_pic_query : function(req, ref){
        var psql_query = "INSERT INTO sentpics (referrer, timestamp, requeststring) values ('"+ref+"', CURRENT_TIMESTAMP,'"+req.toString()+"')";
        console.log(psql_query);
        return psql_query;
    },

    sign_up_query : function(req, ref){
        var psql_query = "INSERT INTO sentpics (referrer, timestamp, requeststring) values ('"+ref+"', CURRENT_TIMESTAMP,'"+req.toString()+"')";
        console.log(psql_query);
        return psql_query;
    },

    sign_in_query : function(req, ref){
        var psql_query = "INSERT INTO sentpics (referrer, timestamp, requeststring) values ('"+ref+"', CURRENT_TIMESTAMP,'"+req.toString()+"')";
        console.log(psql_query);
        return psql_query;
    },

    get_if_in_db : function(uname, email){
        var psql_query = "SELECT count(*) FROM users WHERE username='"+uname+"' or email='"+email+"'";
        console.log(psql_query);
        return psql_query;
    },

    signup_user : function(name, uname, passh, email, created_at, updated_at, cookie, AESkey, token){
        var psql_query = "INSERT INTO users (name, username, password_hash, email, created_at_int, updated_at_int, cookie, aeskey, token) VALUES " +
            "('"+name+"', '"+uname+"', '"+passh+"', '"+email+"', '"+created_at+"', '"+updated_at+"', '"+cookie+"', '"+AESkey+"', '"+token+"')";
        return psql_query;
    },

    find_user : function(cookie){
        var psql_query = "SELECT user_id, username, email, name FROM users WHERE cookie='"+cookie+"'";
        return psql_query;
    },

    signin_user : function(username, pass_hash){
        //var psql_q = "UPDATE users SET cookie WHERE (username='"+username+"' or email='"+username+"') and password_hash='"+pass_hash+"'"
        var psql_query = "SELECT user_id, username FROM users WHERE (username='"+username+"' or email='"+username+"') and password_hash='"+pass_hash+"'";
        return psql_query;
    },

    update_cookie_for_signed_in_user : function (userid, cookie, updated_at) {
        var psql_query = "UPDATE users SET cookie='"+cookie+"', updated_at_int='"+updated_at+"' WHERE user_id='"+userid+"'";
        return psql_query;
    },

    get_keys_for_user : function(cookie){
        var psql_query = "SELECT aeskey, token FROM users WHERE cookie='"+cookie+"'";
        return psql_query;
    },

    get_pic_stats_of_a_user : function(token){
        var psql_query = "SELECT * FROM pic as p join users as u on p.user_id=u.user_id WHERE (u.token='"+token+"' or u.cookie='"+token+"')";
        return psql_query;
    },

    get_ref_stats_of_a_user : function(token){
        var psql_query = "SELECT * FROM ref as r join users as u on r.user_id=u.user_id WHERE (u.token='"+token+"' or u.cookie='"+token+"')";
        return psql_query;
    },
    create_new_campaign : function(user_id, name, notes, key, secret, timestamp){
        var psql_query = "INSERT INTO camps (user_id, name, notes, hash, secret, timestamp) VALUES ('"+user_id+"', '"+name+"', '"+notes+"', '"+key+"', '"+secret+"', '"+timestamp+"')";
        return psql_query;
    },
    get_campaigns : function (user_id) {
        var psql_query = "SELECT * from camps where user_id='"+user_id+"' ORDER BY timestamp DESC";
        return psql_query;
    },
    get_symptom_query : function(illness, since_days, description, filename, consulting_doc, payment, unix_stamp, user_id){
        var psql_query = "INSERT INTO consult (illness, since_days, description, filename, payment, physician, user_id, timestamp) VALUES ('"+illness+"', '"+since_days+"', '"+description+"', '"+filename+"', '"+payment+"', '"+consulting_doc+"', '"+user_id+"', '"+unix_stamp+"') RETURNING id";
        return psql_query;
    },
    add_to_cases : function(case_id){
        var date = new Date();
        var unix_stamp = moment().valueOf(date);
        var psql_query = "INSERT INTO process (case_id, state, specialization, timestamp) VALUES ('"+case_id+"', 0, 'GP', '"+unix_stamp+"')";
        return psql_query;
    },
    logit : function (name) {
        var date = new Date();
        var unix_stamp = moment().valueOf(date);
        var psql_query = "INSERT INTO logs (name, timestamp) VALUES ('"+name+"', '"+unix_stamp+"')";
        return psql_query;
    },
    get_case_patient_details : function (case_id) {
        var psql_query = "SELECT u.name, u.username, u.email from consult as c join users as u on u.user_id=c.user_id where c.id='"+case_id+"' ORDER BY timestamp DESC";
        return psql_query;
    },
    create_new_payment : function (payment_request_id, name, email, phone, longurl, shorturl, redirect_url, webhook, timestamp, amount, case_id) {
        var psql_query = "INSERT INTO payments (payment_request_id, name, email, phone, longurl, shorturl, redirect_url, webhook, timestamp, amount, case_id) VALUES ('"+payment_request_id+"', '"+name+"', '"+email+"', '"+phone+"', '"+longurl+"', '"+shorturl+"', '"+redirect_url+"', '"+webhook+"', '"+timestamp+"', '"+amount+"', '"+case_id+"')";
        return psql_query;
    },
    update_payments :function (payment_request_id, payment_id, fees, amount, status, shorturl, currency, updated_at) {
        var psql_query = "UPDATE payments SET shorturl='"+shorturl+"', payment_id='"+payment_id+"', fees='"+fees+"', amount='"+amount+"', status='"+status+"', currency='"+currency+"', update_timestamp='"+updated_at+"' WHERE payment_request_id='"+payment_request_id+"' RETURNING case_id, status";
        return psql_query;
    },
    payment_status : function (payment_request_id, payment_id) {
        var psql_query = "SELECT status, case_id FROM payments WHERE payment_request_id='"+payment_request_id+"' and payment_id='"+payment_id+"'";
        return psql_query;
    },
    get_case_page_case_details : function (case_id){
        var psql_query = "SELECT c.physician, u.name, u.username as phonenumber, description FROM consult c join users u on c.user_id=u.user_id WHERE c.id='"+case_id+"'";
        return psql_query;
    }
};

module.exports.queries = queries;
