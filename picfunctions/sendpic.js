var fs = require('fs');
var executePSqlQuery = require('../lib/psql');
var queries = require('../queries/queries').queries;

var render_pic = function(req, res, next){
    console.log("rendering pic");
    var ref = 1;
    console.log(ref);
    var img = fs.readFileSync(__dirname + '/../public/f60.png');
    res.writeHead(200, {'Content-Type': 'image/png' });
    res.end(img, 'binary');
    record_pic_render(req);
};

var record_pic_render = function(req){
    var ref = req.query.referrer || 'none';
    executePSqlQuery.pSqlPromise(queries.record_pic_query(req, ref)).then(function(req){
        console.log("Success: "+req);
    }).catch(function(err){
        console.log("Error: "+err);
    });
};

module.exports.render_pic = render_pic;