var express = require("express");
var app = express();
var fs = require('fs');
var router = express.Router();
var pg = require('pg');
var executePSqlQuery = require('./psql');
var Promise = require("bluebird");
var sendpic = require('../picfunctions/sendpic');
var auth = require('../auth/authentication.js').auth;
var auth_crypto = require('../auth/auth_helper');
var nodemailer = require('nodemailer');
var queries = require('../queries/queries.js').queries;
var mod = require('../themod/index');
var moment = require('moment');
var sec = "#3c8F5";
var sec1 = "5u110^";
var crypto = require('crypto');
var path = require('path');
var multer  = require('multer');
var http = require('http');
var requestlib = require('request');
var querystring = require('querystring');

function update_payments_promise(payment_request_id, payment_id, fees, amount, status, shorturl, currency, updated_at){
    return new Promise(function(resolve, reject){
        executePSqlQuery.pSqlPromise(queries.update_payments(payment_request_id, payment_id, fees, amount, status, shorturl, currency, updated_at)).then(function(update_payments_result){
        	if(update_payments_result.length > 0){
				resolve({
        			status : update_payments_result[0].status,
        			case_id : update_payments_result[0].case_id
        		});
        	} else {
        		reject(update_payments_error);	
        	}
        }).catch(function(update_payments_error){
            reject(update_payments_error);
        });
    });
}

module.exports.update_payments_promise = update_payments_promise;