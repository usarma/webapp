var pg = require("pg");
var Promise = require("bluebird");

function pSqlPromise(query){
    return new Promise(function(resolve, reject){
        //console.log(process.env.DATABASE_URL);
        //var pgurl = process.env.DATABASE_URL === undefined ? 'postgres://postgres@localhost/udaykrishna' : process.env.DATABASE_URL ;
        //console.log()
        //var pgurl = process.env.DATABASE_URL;// === undefined ? 'postgres://postgres@localhost/udaykrishna' : process.env.DATABASE_URL ;
        pg.connect(global.pgurl, function(err, client, done) {
            if(err){
                console.log(err);
                reject(err);
            }
            client.query(query, function(err, result) {
                done();
                if (err)
                {
                    console.log(err);
                    reject(err);
                }
                else
                {
                    resolve(result.rows);
                }
            });
        });
    })
}

module.exports.pSqlPromise = pSqlPromise;