var express = require('express');
var exphbs = require('express-secure-handlebars');
var routes = require('./routes/routes');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var path = require('path');
var url = require('url');
global.appRoot = path.resolve(__dirname);
var app = express();
var crypto = require("crypto");
var auth = require('./auth/authentication.js').auth;
app.use(bodyParser.json());  // to support JSON-encoded bodies
if(process.env.ENV === "prod"){
    global.pgurl = 'postgres://cureoc:cureoc@localhost/custapp';
} else {
    global.pgurl = 'postgres://cureoc:cureoc@35.154.139.170:5432/custapp';
}
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true
}));
app.use(cookieParser());
app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');
app.use(express.static(__dirname + '/public', {maxAge: 86400000}));
const non_auth_routes = ['/signin', '/signup', '/', '/test', '/instamojo_webhook'];
const opt_auth_routes = ['/consult'];
const auth_routes = ['/consult', '/postpayment'];
app.use((req, res, next) => {
    console.log(req.url);
    if (non_auth_routes.indexOf(url.parse(req.url).pathname) >= 0) {
        next();
    }
    else {
        auth.get_user_from_cookie(req.cookies.mycookie, res)
            .then(() => next())
            .catch(() => {
                res.redirect('/signin');
            });
    }
});
console.log(global.pgurl);
app.use('/', routes.router);
//auth middleware
app.set('port', (process.env.PORT || 50101));

app.listen(app.get('port'), function () {
    console.log('Node app is running on port', app.get('port'));
});
