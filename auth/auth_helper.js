var crypto = require('crypto');
var cryptoAES = require('crypto'),
    algorithm = 'aes-256-ctr';

module.exports = {
    cookie : function(password, date){
        var hash = date+'j!Eq2J84_CbtXRCR9fpr*6PnP4@xR&d$-zdd=5+C-jWhXBh#+c'+password;
        return hash;
    },

    encrypt : function (text){
        var hash = crypto.createHash('sha256').update('uZ_kD6@YkNK+?pH3@@A?FhyjJ'+text+'p#HFkT8FM8tR&w-pv-N4YHyA').digest('hex');
        return hash;
    },

    encryptAES : function(text, password){
        var cipher = cryptoAES.createCipher(algorithm,password);
        var crypted = cipher.update(text,'utf8','hex');
        crypted += cipher.final('hex');
        return crypted;
    },

    decryptAES : function(text, password){
        var decipher = cryptoAES.createDecipher(algorithm,password);
        var dec = decipher.update(text,'hex','utf8');
        dec += decipher.final('utf8');
        return dec;
    }
};
