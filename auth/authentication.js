var pg = require("pg");
var Promise = require("bluebird");
var queries = require('../queries/queries').queries;
var executePSqlQuery = require('../lib/psql');
var xssFilters = require('xss-filters');
var moment = require('moment');
var authHelper = require('./auth_helper.js');
var redis = require('promise-redis')();
var client = ""
if(process.env.ENV === "prod"){
    client = redis.createClient();
} else {
    client = redis.createClient(6379, "35.154.139.170");
}
var crypto = require('crypto');
var express = require('express');
var app = express();

var auth = {
    validate_and_sign_up_user : function(name, phone, email, password, cpassword){
        return new Promise(function(resolve, reject){
            phone = xssFilters.inHTMLData(phone);
            email = xssFilters.inHTMLData(email);
            password = xssFilters.inHTMLData(password);
            cpassword = xssFilters.inHTMLData(cpassword);
            var signup = {
                error : [],
                done : false
            };
            if(!name) signup.error.push("phone number is empty");
            if(!phone) signup.error.push("phone number is empty");
            if(!email) signup.error.push("email is empty");
            if(!password) signup.error.push("password is empty");
            if(signup.error.length > 0){
                resolve(signup);
                return false;
            }
            //check existance in database
            console.log(queries.get_if_in_db(phone, email));
            executePSqlQuery.pSqlPromise(queries.get_if_in_db(phone, email)).then(function(results){
                if(results[0].count === "0" || results[0].count === 0){
                    var date = new Date();
                    var unix_stamp = moment().valueOf(date);
                    var pass_hash = authHelper.encrypt(password);
                    var cookie = authHelper.encrypt(authHelper.cookie(password, date));
                    var aeskey = crypto.randomBytes(20).toString('hex');
                    var token = crypto.randomBytes(8).toString('hex');
                    var sql_query = queries.signup_user(name, phone, pass_hash, email, unix_stamp, unix_stamp, cookie, aeskey, token);
                    console.log(sql_query);
                    executePSqlQuery.pSqlPromise(sql_query).then(function(results){
                        signup.cookie = cookie;
                        signup.aeskey = aeskey;
                        signup.token = token;
                        signup.done = true;
                        resolve(signup);
                        return false;
                    }).catch(function(err){
                        reject(err);
                        return false;
                    });
                    //moment().valueOf(new Date())
                }else{
                    signup.error.push("username/email already exists");
                    resolve(signup);
                    return false;
                }
            }).catch(function(err){
                reject(err);
            });
        });
    },

    sign_user_in : function(username, password){
        return new Promise(function(resolve, reject){
            username = xssFilters.inHTMLData(username);
            password = xssFilters.inHTMLData(password);
            var signup = {
                error : [],
                done : false
            };
            if(!username) signup.error.push("username/email is empty");
            if(!password) signup.error.push("password is empty");
            if(signup.error.length > 0){
                resolve(signup);
                return false;
            }
            var pass_hash = authHelper.encrypt(password);
            var date = new Date();
            var cookie = authHelper.encrypt(authHelper.cookie(password, date));
            var unix_stamp = moment().valueOf(date);
            executePSqlQuery.pSqlPromise(queries.signin_user(username, pass_hash)).then(function(results){
                if(results.length === 0){
                    signup.error.push("username/password not match");
                    resolve(signup);
                }
                var userid = results[0].user_id;
                var username = results[0].username;
                executePSqlQuery.pSqlPromise(queries.update_cookie_for_signed_in_user(userid, cookie, unix_stamp)).then(function(results){
                    resolve({cookie : cookie, username : username, user_id : userid});
                }).catch(function(err){
                    reject(err);
                });
            }).catch(function(err){
                reject(err);
            });
        });
    },

    get_user_from_cookie : function(cookie, res){
        return new Promise(function(resolve, reject){
            executePSqlQuery.pSqlPromise(queries.find_user(cookie)).then(function(results){
                console.log(queries.find_user(cookie));
                console.log(results.length);
                if(results.length > 0){
                    res.locals.user = {
                        user_id: results[0].user_id,
                        username: results[0].username,
                        name: results[0].name,
                        email: results[0].email
                    };
                    resolve(results[0]);
                }
                reject({user : "No such user"});
            }).catch(function(err){
                reject(err);
            });
        });
    },

    add_symptoms_for_consultation : function(req, res){
        return new Promise(function(resolve, reject){
            var filename = req.file.filename;
            var consulting_doc = req.body.specialist.trim();
            var illness = req.body.illness.trim();
            var since_days = req.body.since.trim();
            var description = req.body.desc.trim();
            var payment = 0;
            var date = new Date();
            var unix_stamp = moment().valueOf(date);
            console.log(res.locals.user);
            //console.log(queries.get_symptom_query(illness, since_days, description, filename, consulting_doc, payment, unix_stamp, res.locals.user.user_id));
            executePSqlQuery.pSqlPromise(queries.get_symptom_query(illness, since_days, description, filename, consulting_doc, payment, unix_stamp, res.locals.user.user_id)).then(function(results){
                // add to redis here
                client.lpush('consult_'+consulting_doc+'list', results[0].id).then(function (data) {
                    resolve(results);
                });
            }).catch(function(err){
                reject(err);
            });
        });
    }
};

module.exports.auth = auth;
